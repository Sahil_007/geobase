# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:

import scrapy


class GeobaseItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    loc=scrapy.Field()
    elevation=scrapy.Field()
    timezone=scrapy.Field()
    county=scrapy.Field()
    typeOfLocation=scrapy.Field()
    population=scrapy.Field()
    alternateNames=scrapy.Field()
    postCode=scrapy.Field()
    url=scrapy.Field()
    cityName=scrapy.Field()
    countryName=scrapy.Field()
    cityPopulationFacts=scrapy.Field()

