import scrapy
from bs4 import BeautifulSoup
import functools
from pyvirtualdisplay import Display
import re
from scrapy import FormRequest
import requests
from geobase.items import GeobaseItem
from selenium import webdriver
class GeobaseSpider(scrapy.Spider):
	name="geo"
	start_urls=["http://geoba.se/location.php?query=khanna"]
	#start_urls=["http://geoba.se/location.php?query=arinsal"]
	#start_urls=["http://geoba.se/location.php?query=andorra%20la%20vella"]
	#start_urls=["http://geoba.se/location.php?query=s%C5%8Dzmah%20qal%E2%80%98ah"]
	
	def parse(self,response):
		
		cityName=response.css('h1 strong::text')[0].extract()
		country_name=response.css('h1::text').extract()[-1].replace(",","").strip()
			
		item=GeobaseItem()
		display = Display(visible=0, size=(800, 600))
		display.start()	
		driver=webdriver.Firefox()
		driver.get(response.url)
		soup=BeautifulSoup(driver.page_source,'html.parser')
		table=soup.find('table')
		tr_list=table.findAll('tr')
		lat=''
		lon=''
		
		for each in tr_list:
			b_tag_content=each.find('b')
			try:
				is_lat=re.search('Latitude',b_tag_content.text)
				if is_lat:
					lat=float(each.find('font').text.split("(")[-1].replace(")","").strip())
					break
				else:
					lat=''
				
							
			except:
				a=''

		for each in tr_list:
			b_tag_content=each.find('b')
			try:
				is_long=re.search('Longitude',b_tag_content.text)		
				if is_long:
					lon=float(each.find('font').text.split("(")[-1].replace(")","").strip())
					break
				else:
					lon=''
			except:
				a=''
		item['loc']={'lat':lat,'lng':lon}		
		for each in tr_list:
			b_tag_content=each.find('b')
			try:
				is_elev=re.search('Elevation',b_tag_content.text)
				if is_elev:
					item['elevation']=int(each.find('font').text.split("/")[-1].replace("meters","").strip())
					break	
				else:
					item['elevation']=''			
			except:
				a=''

		for each in tr_list:
			b_tag_content=each.find('b')
			try:
				is_time=re.search('Timezone',b_tag_content.text)
				if is_time:
					item['timezone']=tr_list[5].find('font').text.split("(")[-1].replace(")","")
					break	
				else:
					item['timezone']=''			
			except:
				a=''		

		for each in tr_list:
			b_tag_content=each.find('b')
			try:
				is_post=re.search('Post Code',b_tag_content.text)
				if is_post:
					item['postCode']=each.find('font').text
					break	
				else:
					item['postCode']=''			
			except:
				a=''		

						
		for each in tr_list:
			b_tag_content=each.find('b')
			try:
				is_pop=re.search('Population',b_tag_content.text)
				if is_pop:
					item['population']=int(each.find('font').text.replace(",",""))	
					break	
				else:
					item['population']=''
			except:
				a=''

		for each in tr_list:
			b_tag_content=each.find('b')
			try:
				is_pop=re.search('Population',b_tag_content.text)
				if is_pop:
					temp_fact_array=each.findAll('font')[-1]
					countryRank=''
					continentRank=''
					worldRank=''
					a_list=temp_fact_array.findAll('a')
					is_largest_in_country=re.search('Largest',a_list[0].text)
					if is_largest_in_country:
						countryRank=1
					else:
						countryRank=int(re.findall('\d+',a_list[0].text.replace(",",""))[0])
					is_largest_in_continent=re.search('Largest',a_list[1].text)
					if is_largest_in_continent:
						continentRank=1
					else:
						continentRank=int(re.findall('\d+',a_list[1].text.replace(",",""))[0])
					is_largest_in_world=re.search('Largest',a_list[2].text)	

					if is_largest_in_world:
						worldRank=1
					else:	
						worldRank=int(re.findall('\d+',a_list[2].text.replace(",",""))[0])
					item['cityPopulationFacts']={"countryRank":countryRank,"continentRank":continentRank,"worldRank":worldRank}
						
					break	
				else:
					
					item['cityPopulationFacts']={"countryRank":"","continentRank":"","worldRank":""}

			except:
				a=''				

		for each in tr_list:
			b_tag_content=each.find('b')
			try:
				is_alt=re.search('Alternate Names',b_tag_content.text)
				if is_alt:
					alternateNames=each.find('font').text.split(",")
					alternateNames=[each.encode("utf-8") for each in alternateNames]
					item['alternateNames']=alternateNames
					break
				else:
					item['alternateNames']=''			
			except:
				a=''		
		

		for each in tr_list:
			b_tag_content=each.find('b')
			try:
				is_count=re.search('County',b_tag_content.text)
				if is_count:
					item['county']=each.find('font').text
					break
				else:
					item['county']=''			
			except:
				a=''

		for each in tr_list:
			b_tag_content=each.find('b')
			try:
				is_loc=re.search('Type of Location',b_tag_content.text)
				if is_loc:
					item['typeOfLocation']=each.find('font').text
					break
				else:
					item['typeOfLocation']=''			
			except:
				a=''
		item['url']=response.url
		item['cityName']=cityName
		item['countryName']=country_name

		yield item		
			
		
		file=open("cities1000.txt","r")
		for each in file:
			city=each.split("\t")[1].lower()
			url="http://geoba.se/location.php?query="+str(city)
			yield scrapy.Request(url,self.parse_other_cities)

	def parse_other_cities(self,response):
		cityName=response.css('h1 strong::text')[0].extract()
		country_name=response.css('h1::text').extract()[-1].replace(",","").strip()
			
		item=GeobaseItem()
		display = Display(visible=0, size=(800, 600))
		display.start()	
		driver=webdriver.Firefox()
		driver.get(response.url)
		soup=BeautifulSoup(driver.page_source,'html.parser')
		table=soup.find('table')
		tr_list=table.findAll('tr')
		lat=''
		lon=''
		
		for each in tr_list:
			b_tag_content=each.find('b')
			try:
				is_lat=re.search('Latitude',b_tag_content.text)
				if is_lat:
					lat=float(each.find('font').text.split("(")[-1].replace(")","").strip())
					break
				else:
					lat=''
				
							
			except:
				a=''

		for each in tr_list:
			b_tag_content=each.find('b')
			try:
				is_long=re.search('Longitude',b_tag_content.text)		
				if is_long:
					lon=float(each.find('font').text.split("(")[-1].replace(")","").strip())
					break
				else:
					lon=''
			except:
				a=''
		item['loc']={'lat':lat,'lng':lon}		
		for each in tr_list:
			b_tag_content=each.find('b')
			try:
				is_elev=re.search('Elevation',b_tag_content.text)
				if is_elev:
					item['elevation']=int(each.find('font').text.split("/")[-1].replace("meters","").strip())
					break	
				else:
					item['elevation']=''			
			except:
				a=''

		for each in tr_list:
			b_tag_content=each.find('b')
			try:
				is_time=re.search('Timezone',b_tag_content.text)
				if is_time:
					item['timezone']=tr_list[5].find('font').text.split("(")[-1].replace(")","")
					break	
				else:
					item['timezone']=''			
			except:
				a=''		

		for each in tr_list:
			b_tag_content=each.find('b')
			try:
				is_post=re.search('Post Code',b_tag_content.text)
				if is_post:
					item['postCode']=each.find('font').text
					break	
				else:
					item['postCode']=''			
			except:
				a=''		

						
		for each in tr_list:
			b_tag_content=each.find('b')
			try:
				is_pop=re.search('Population',b_tag_content.text)
				if is_pop:
					item['population']=int(each.find('font').text.replace(",",""))	
					break	
				else:
					item['population']=''
			except:
				a=''

		for each in tr_list:
			b_tag_content=each.find('b')
			try:
				is_pop=re.search('Population',b_tag_content.text)
				if is_pop:
					temp_fact_array=each.findAll('font')[-1]
					countryRank=''
					continentRank=''
					worldRank=''
					a_list=temp_fact_array.findAll('a')
					is_largest_in_country=re.search('Largest',a_list[0].text)
					if is_largest_in_country:
						countryRank=1
					else:
						countryRank=int(re.findall('\d+',a_list[0].text.replace(",",""))[0])
					is_largest_in_continent=re.search('Largest',a_list[1].text)
					if is_largest_in_continent:
						continentRank=1
					else:
						continentRank=int(re.findall('\d+',a_list[1].text.replace(",",""))[0])
					is_largest_in_world=re.search('Largest',a_list[2].text)	

					if is_largest_in_world:
						worldRank=1
					else:	
						worldRank=int(re.findall('\d+',a_list[2].text.replace(",",""))[0])
					item['cityPopulationFacts']={"countryRank":countryRank,"continentRank":continentRank,"worldRank":worldRank}
						
					break	
				else:
					
					item['cityPopulationFacts']={"countryRank":"","continentRank":"","worldRank":""}

			except:
				a=''				

				
		for each in tr_list:
			b_tag_content=each.find('b')
			try:
				is_alt=re.search('Alternate Names',b_tag_content.text)
				if is_alt:
					alternateNames=each.find('font').text.split(",")
					alternateNames=[each.encode("utf-8") for each in alternateNames]
					item['alternateNames']=alternateNames
					break
				else:
					item['alternateNames']=''			
			except:
				a=''

		for each in tr_list:
			b_tag_content=each.find('b')
			try:
				is_count=re.search('County',b_tag_content.text)
				if is_count:
					item['county']=each.find('font').text
					break
				else:
					item['county']=''			
			except:
				a=''

		for each in tr_list:
			b_tag_content=each.find('b')
			try:
				is_loc=re.search('Type of Location',b_tag_content.text)
				if is_loc:
					item['typeOfLocation']=each.find('font').text
					break
				else:
					item['typeOfLocation']=''			
			except:
				a=''
		item['url']=response.url
		item['cityName']=cityName
		item['countryName']=country_name

		yield item
		
			
		
			
				
		
		
		